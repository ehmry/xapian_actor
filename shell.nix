{ pkgs ? import <nixpkgs> { } }:
pkgs.nim2Packages.buildNimPackage {
  name = "dummy";
  propagatedNativeBuildInputs = [ pkgs.pkg-config ];
  propagatedBuildInputs = [ pkgs.xapian ];
}
