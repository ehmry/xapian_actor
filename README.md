# sqlite_actor

Syndicate actor for accessing Xapian databases.

Provides a Xapian binding module at [src/xapian_actor/xapian.nim](./src/xapian_actor/xapian.nim).
