author = "Emery Hemingway"
backend = "cpp"
bin = @["xapian_actor"]
description = "Syndicate actor for accessing Xapian databases"
license = "Unlicense"
srcDir = "src"
version = "20230610"

requires "nim >= 1.6.4", "syndicate >= 20230609"
