
import
  preserves, std/tables

type
  Document* {.preservesRecord: "document".} = object
    `id`*: BiggestInt
    `data`*: string

  Match* {.preservesRecord: "match".} = object
    `label`*: Preserve[void]
    `docid`*: BiggestInt
    `weight`*: float32
    `rank`*: BiggestInt

  ValueSlots* = Table[BiggestInt, Preserve[void]]
  Query* {.preservesRecord: "query".} = object
    `label`*: Preserve[void]
    `expression`*: string
    `lang`*: Symbol

  DatabaseInfo* {.preservesRecord: "xapian".} = object
    `uuid`*: string
    `path`*: string

proc `$`*(x: Document | Match | ValueSlots | Query | DatabaseInfo): string =
  `$`(toPreserve(x))

proc encode*(x: Document | Match | ValueSlots | Query | DatabaseInfo): seq[byte] =
  encode(toPreserve(x))
