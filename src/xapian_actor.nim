# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import preserves, syndicate
import ./xapian_actor/[protocol, xapian]

type
  Value = Preserve[void]
  DatabaseArg {.preservesDictionary.} = object
    database: string
  DataspaceArg {.preservesDictionary.} = object
    dataspace: Ref

runActor("main") do (root: Ref; turn: var Turn):
  connectStdio(root, turn)

  during(turn, root, ?DatabaseArg) do (path: string):
    let db = initDatabase(path)

    during(turn, root, ?DataspaceArg) do (ds: Ref):
      discard publish(turn, ds, DatabaseInfo(uuid: db.uuid, path: path))

      during(turn, ds, ?Observe(pattern: !protocol.Document) ?? {0: grabLit()}) do (id: BiggestInt):
        discard publish(turn, ds, protocol.Document(
            id: id,
            data: $db.getDocument(DocId id).data,
          ))

      during(turn, ds, ?protocol.Query) do (label: Value, expr: string, lang: Symbol):
        let enq = initEnquire(db)
        block:
          let qp = initQueryParser()
          let stem = initStem(string lang)
          qp.setStemmer(stem)
          qp.setStemmingStrategy(STEM_SOME);
          qp.setDatabase(db)
          enq.setQuery(qp.parseQuery(expr))
        block:
          let matches = enq.getMset(0, db.getDocCount)
          for e in matches:
            discard publish(turn, ds, Match(
                label: label,
                docid: BiggestInt e.docid,
                weight: e.weight,
                rank: BiggestInt e.rank,
              ))
  do:
    close(db)
      # close database on path retraction
